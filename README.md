## Simple ListView Example using Flutter
----------------------------------------
> This app includes simple ListView that have many items and one or more of them can be selected as favourite and then we can see liked elements.

**Main widgets and elements used in this app are the followings:**

1. StateLess and StateFull Widgets
2. Material App, Scaffold Widget, AppBar
3. english_words library
4. Using sets and lists in Dart language
5. Text and TextyStyle Widgets
6. Iconbutton Widget
7. ListView, ListTile and Divider Widgets
8. Using Navigator (push method)

------------------------------------------

>This app is provided from (https://codelabs.developers.google.com/codelabs/first-flutter-app-pt1/index.html?index=..%2F..index#0)

## Screenshots of app:

![picture](https://bitbucket.org/KananYusubov/simpleflutterlistexample/raw/632b90d1c26465b2d4b41b1647cd59acfbbf50fa/first.png) 
![picture](https://bitbucket.org/KananYusubov/simpleflutterlistexample/raw/632b90d1c26465b2d4b41b1647cd59acfbbf50fa/second.png)
![picture](https://bitbucket.org/KananYusubov/simpleflutterlistexample/raw/632b90d1c26465b2d4b41b1647cd59acfbbf50fa/third.png)
